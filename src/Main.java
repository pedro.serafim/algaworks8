public class Main {

    public static void main(String[] args) {
        Fornecedor imobiliaria = new Fornecedor();
        imobiliaria.setNome("Visão Imobiliária");

        Fornecedor mercado = new Fornecedor();
        mercado.setNome("Mercado do Pedro");

        ContaPagar conta1 = new ContaPagar();
        conta1.setDescricao("o aluguel da matriz");
        conta1.setValor(1230d);
        conta1.setDataVencimento("10/05/2019");
        conta1.setFornecedor(imobiliaria);

        ContaPagar conta2 = new ContaPagar(mercado, "as compras do mês", 390d, "19/05/2019");
        ContaPagar conta3 = new ContaPagar(mercado, "o aluguel da filial", 700d, "11/05/2019");

        System.out.println("Contas a Pagar:");
        conta1.pagar();
        conta2.pagar();
        conta3.pagar();
    }
}
